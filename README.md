# FC30HTMLCSS
Przydatne linki oraz dodatkowe materiały z kursu HTML & CSS

## Emmet
https://docs.emmet.io/abbreviations/syntax/

## Najbardziej popularnyme właściwościami CSS:
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference

## Kaskadowość oraz dziedziczenie styli:
https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance

## CSS order
https://css-tricks.com/poll-results-how-do-you-order-your-css-properties/?fbclid=IwAR1Nsbpgs9JinBxRhj3TbO6BdsJh_0rxz2InJZ7Me49m0RGwq_H_bAnbfWo

## GRID
https://css-tricks.com/snippets/css/complete-guide-grid/

### Gra grid dla ćwiczeń
https://cssgridgarden.com/#pl

## FLEXBOX
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

### Gra Flex dla ćwiczeń
https://flexboxfroggy.com/#pl


## Miejsca z darmowymi materiałami - zdjęcia, grafiki, ikony, czcionki
- https://icons8.com/icons/set/x-sign
- https://pixabay.com/pl/
- https://fonts.google.com/
- https://fontawesome.com/
- https://www.lipsum.com/
- https://picsum.photos/

## Reset CSS
https://meyerweb.com/eric/tools/css/reset/

## Element link
https://developer.mozilla.org/pl/docs/Web/HTML/Element/link

## BEM
https://css-tricks.com/bem-101/